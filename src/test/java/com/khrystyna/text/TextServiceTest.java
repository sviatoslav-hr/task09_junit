package com.khrystyna.text;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.*;

class TextServiceTest {

    @Test
    void getMaxSameWordsSentence() {
        String sentence = "This is test sentence! Find sentence with max same words number. That is is it! Good Luck!";
        TextService service = new TextService(sentence);
        String result = service.getMaxSameWordsSentence();
        String expected = "That is is it!";
        Assert.assertEquals(result, expected);
    }

    @Test
    void getSortedByWordsNumber() {
        String sentence = "This is test sentence! Find sentence with max same words number. That is is it! Good Luck!";
        TextService service = new TextService(sentence);
        List<String> result = service.getSortedByWordsNumber();
        List<String> expected = new ArrayList<>();
        expected.add("Good Luck!");
        expected.add("That is is it!");
        expected.add("This is test sentence!");
        expected.add("Find sentence with max same words number.");
        Assert.assertEquals(expected, result);
    }

    @Test
    void getUniqueWords() {
        String sentence = "This is test sentence! Find good sentence with luck.\nGood is Luck!";
        TextService service = new TextService(sentence);
        Set<String> result = service.getUniqueWords();
        Set<String> expected = new HashSet<>();
        expected.add("this");
        expected.add("test");
        expected.add("find");
        expected.add("with");
        Assert.assertEquals(expected, result);
    }

    @Test
    void getWordsOfGivenLengthInQuestionSentences() {
    }

    @Test
    void replaceFirstVowelWordsWithLongest() {
    }

    @Test
    void getSortedWordsAsc() {
    }

    @Test
    void getSortedWordsByVowelPercentage() {
    }

    @Test
    void getVowelWordsSorted() {
    }

    @Test
    void getWordsSortedByCharNum() {
    }

    @Test
    void getWordsSortedByOccurrence() {
    }
}
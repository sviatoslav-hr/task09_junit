package com.khrystyna.text;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * This class contains {@link String} value of sentence and
 * {@link java.util.Map} of words and their number in the sentence.
 *
 * @author Sviatoslav Khrystyna
 * @version 1.0
 * @since 02.05.2019
 */
public class Sentence {
    /**
     * List of words  in the sentence.
     */
    private List<String> words;
    /**
     * String value of the sentence.
     */
    private String stringValue;

    /**
     * Constructs a new sentence.
     *
     * @param sentence string value to be divided into words
     */
    public Sentence(final String sentence) {
        stringValue = sentence.trim();
        words = Arrays.asList(splitToWords(sentence));
    }

    /**
     * @return words list
     */
    public final List<String> getWords() {
        return words;
    }

    /**
     * @param str string to be divided
     * @return string array by dividing into words
     */
    private String[] splitToWords(final String str) {
        return str.split("(?!\\w).");
    }

    public long wordEntries(String word) {
        return words.stream()
                .filter(s -> s.equalsIgnoreCase(word))
                .count();
    }

    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Sentence sentence = (Sentence) o;
        return Objects.equals(words, sentence.words)
                && Objects.equals(stringValue, sentence.stringValue);
    }

    @Override
    public final int hashCode() {
        return Objects.hash(words, stringValue);
    }

    @Override
    public final String toString() {
        return stringValue;
    }
}
